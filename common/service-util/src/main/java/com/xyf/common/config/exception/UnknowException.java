package com.xyf.common.config.exception;

import com.xyf.common.result.ResultCodeEnum;
import lombok.Data;

@Data
public class UnknowException extends RuntimeException{

    private Integer code;//状态码

    private String msg;//描述信息

    public UnknowException( Integer code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    /**
     * 接收枚举类型对象
     * @param resultCodeEnum
     */
    public UnknowException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
        this.msg = resultCodeEnum.getMessage();
    }

    @Override
    public String toString() {
        return "UnknowException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }
}
