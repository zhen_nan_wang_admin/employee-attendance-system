package com.xyf.common.result;


import lombok.Data;

@Data
public class Result<T> {

    private Integer code;//状态码

    private String message;//返回信息

    private Integer totalSize;//数据条数

    private T data;//数据

    //私有化
    private Result(){}

    //封装返回的数据
    public static <T> Result<T> build(T body, ResultCodeEnum resultCodeEnum,Integer totalSize) {
        Result<T> result = new Result<>();
        //封装数据
        if(body != null){
            result.setData(body);
        }
        if(totalSize != null){
            result.setTotalSize(totalSize);
        }
        //状态码
        result.setCode(resultCodeEnum.getCode());
        //相关返回信息
        result.setMessage(resultCodeEnum.getMessage());
        return result;
    }

    //返回成功
    public static<T> Result<T> ok(){
        return build(null,ResultCodeEnum.SUCCESS,null);
    }

    public static<T> Result<T> ok(T data){
        return build(data,ResultCodeEnum.SUCCESS,null);
    }

    public static<T> Result<T> ok(T data,Integer totalSize){
        return build(data,ResultCodeEnum.SUCCESS,totalSize);
    }




    //返回失败
    public static<T> Result<T> fail(){
        return build(null,ResultCodeEnum.FAIL,null);
    }

    public static<T> Result<T> fail(T data){
        return build(data,ResultCodeEnum.FAIL,null);
    }

    public static<T> Result<T> fail(T data,Integer totalSize){
        return build(data,ResultCodeEnum.FAIL,totalSize);
    }


    public Result<T> message(String msg){
        this.setMessage(msg);
        return this;
    }

    public Result<T> code(Integer code){
        this.setCode(code);
        return this;
    }

}
