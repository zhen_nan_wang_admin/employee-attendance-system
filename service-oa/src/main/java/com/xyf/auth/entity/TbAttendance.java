package com.xyf.auth.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.time.LocalTime;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbAttendance implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 要求使用UUID
     */
      private String id;

    /**
     * 人员表外键 
     */
    private String userId;

    /**
     * 结果:迟到、早退、请假，病假，正常、旷工，异常（迟到、早退）
     */
    private String result;

    /**
     * 考勤日期
     */
    private LocalDate kqdate;

    /**
     * 上班考勤时间
     */
    private LocalTime chkin;

    /**
     * 下班考勤时间
     */
    private LocalTime chkout;


}
