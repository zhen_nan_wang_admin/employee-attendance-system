package com.xyf.auth.entity;

import java.time.LocalTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbDept implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键  要求使用UUID
     */
      private String id;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 上班规定考勤时间   可设置值为9:00上班
     */
    private LocalTime chkin;

    /**
     * 下班规定考勤时间  可设置值为18:00点下班
     */
    private LocalTime chkout;


}
