package com.xyf.auth.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 要求使用UUID
     */
      private String id;

    /**
     * 人员名称
     */
    @TableField("NAME")
    private String name;

    /**
     * 帐号名
     */
    private String username;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 权限名字  admin为管理员,其他默认people
     */
    private String role;

    /**
     * 部门外键
     */
    private String deptId;


}
