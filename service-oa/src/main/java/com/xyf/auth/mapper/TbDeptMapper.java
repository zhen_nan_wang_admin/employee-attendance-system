package com.xyf.auth.mapper;

import com.xyf.auth.entity.TbDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
public interface TbDeptMapper extends BaseMapper<TbDept> {

}
