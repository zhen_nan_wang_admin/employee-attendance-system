package com.xyf.auth.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xyf.auth.entity.TbUser;
import com.xyf.auth.service.TbUserService;
import com.xyf.common.config.exception.UnknowException;
import com.xyf.common.jwt.JwtHelper;
import com.xyf.common.result.Result;
import com.xyf.common.utils.MD5;
import com.xyf.vo.system.LoginVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 后台登录登出
 * </p>
 */
@Api(tags = "登录管理")
@RestController
@RequestMapping("/admin/system/index")
public class IndexController {

    @Autowired
    private TbUserService tbUserService;

    /**
     * 登录
     *
     * @return
     */
    @PostMapping("login")
    public Result login(@RequestBody LoginVo loginVo) {
        //{"code":200,"data":{"token":"admin-token"}}
//        Map<String, Object> map = new HashMap<>();
//        map.put("token","admin-token");
//        return Result.ok(map);

//        1.获取输入的用户名和密码
//        2.用户信息是否存在
        String username = loginVo.getUsername();
        LambdaQueryWrapper<TbUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TbUser::getUsername, username);
        TbUser sysUser = tbUserService.getOne(wrapper);

        //3.判断用户是否存在
        if (sysUser == null) {
            throw new UnknowException(201, "用户不存在");
        }

        //4.判断密码
        // 取出数据库中的密文密码（MD5）

        String password_dB = sysUser.getPwd();
        String password_input = MD5.encrypt(loginVo.getPassword());
        if (!password_dB.equals(password_input)) {
            throw new UnknowException(201, "密码错误");
        }

        //5.判断用户是否被禁用  1可用0禁用
//        if (sysUser.getStatus().intValue() == 0) {
//            throw new UnknowException(201, "用户已经被禁用");
//        }
        //6.使用jwt根据用户id和用户生成token字符串
        String token = JwtHelper.createToken(sysUser.getId(), sysUser.getUsername());
        //7.返回
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        return Result.ok(map);
    }


    /**
     * 获取用户信息
     *
     * @return
     */
    @GetMapping("info")
    public Result info(HttpServletRequest request) {
        //从请求头获取用户信息（获取请求头token字符串）
        String token = request.getHeader("token");

        //从token字符串获取用户id 和 用户名称
        String userId = JwtHelper.getUserId(token);
        System.out.println(userId);
        //根据用户id查询数据库 把用户信息获取出来
        TbUser sysUser = tbUserService.getById(userId);
        System.out.println(sysUser);
        //返回相应数据
        Map<String, Object> map = new HashMap<>();
        map.put("roles", sysUser.getRole());
        map.put("name", sysUser.getName());

        return Result.ok(map);
    }

    /**
     * 退出
     *
     * @return
     */
    @PostMapping("logout")
    public Result logout() {
        return Result.ok();
    }

}