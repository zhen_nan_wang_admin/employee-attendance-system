package com.xyf.auth.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.xyf.auth.entity.TbAttendance;
import com.xyf.auth.entity.TbDept;
import com.xyf.auth.entity.TbUser;
import com.xyf.auth.service.TbAttendanceService;
import com.xyf.auth.service.TbDeptService;
import com.xyf.auth.service.TbUserService;
import com.xyf.auth.utils.UUIDUtil;
import com.xyf.common.config.exception.UnknowException;
import com.xyf.common.jwt.JwtHelper;
import com.xyf.common.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
@Api(tags = "考勤管理")
@RestController
@RequestMapping("/admin/system/tbAttendance")
public class TbAttendanceController {
    @Autowired
    private TbAttendanceService tbAttendanceService;

    @Autowired
    private TbUserService tbUserService;

    @Autowired
    private TbDeptService tbDeptService;

    @ApiOperation(value = "根据日期获取用户")
    @GetMapping("get/{date}")
    public Result getByDate(@PathVariable String date,HttpServletRequest request) {
        //根据token判断用户角色
        //从请求头获取用户信息（获取请求头token字符串）
        String token = request.getHeader("token");
        if(StringUtils.isEmpty(token)){
            return Result.fail("未登录，无法操作");
        }

        //从token字符串获取用户id 和 用户名称
        String userId = JwtHelper.getUserId(token);
        System.out.println(userId);
        //根据用户id查询数据库 把用户信息获取出来
        TbUser tbUser = tbUserService.getById(userId);

        //判断是否是admin，否则无权进行查询操作
        if(!tbUser.getRole().equals("admin")){
            throw new UnknowException(201,"无权进行该操作");
        }

        LambdaQueryWrapper<TbAttendance> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TbAttendance::getKqdate,date);
        List<TbAttendance> list = tbAttendanceService.list(wrapper);
        return Result.ok(list,list.size());
    }

    @ApiOperation(value = "签到")
    @GetMapping("checkin")
    public Result checkIn(HttpServletRequest request) {
        String token = request.getHeader("token");
        if(StringUtils.isEmpty(token)){
            throw new UnknowException(201,"请登陆后操作");
        }
        //从token字符串获取用户id 和 用户名称
        String userId = JwtHelper.getUserId(token);
        System.out.println(userId);
        //根据用户id查询数据库 把用户信息获取出来
        TbUser tbUser = tbUserService.getById(userId);
        TbAttendance attendance = new TbAttendance();
        attendance.setId(UUIDUtil.getUUID());
        attendance.setUserId(tbUser.getId());

        //设置考勤日期
        attendance.setKqdate(LocalDate.now());

        //把签到时间设为当前
        attendance.setChkin(LocalTime.now());
        attendance.setChkout(null);
        TbDept dept = tbDeptService.getById(tbUser.getDeptId());
        if(LocalTime.now().isAfter(dept.getChkin())){
            attendance.setResult("迟到");
        }else{
            attendance.setResult("正常");
        }


        //禁止重复签到
        //根据当日日期和用户id 查询当日签到信息，在此基础做更新
        List<TbAttendance> list = null;
        try {
            LambdaQueryWrapper<TbAttendance> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(TbAttendance::getKqdate,LocalDate.now());
            wrapper.eq(TbAttendance::getUserId,tbUser.getId());
            list = tbAttendanceService.list(wrapper);
            if (!list.isEmpty()){
                return Result.fail("禁止重复签到");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        if(tbAttendanceService.save(attendance)){
            return Result.ok("签到成功");
        }else {
            return Result.fail();
        }



    }

    @ApiOperation(value = "签退")
    @GetMapping("checkout")
    public Result checkOut(HttpServletRequest request) {
        String token = request.getHeader("token");
        if(StringUtils.isEmpty(token)){
            throw new UnknowException(201,"请登陆后操作");
        }
        //从token字符串获取用户id 和 用户名称
        String userId = JwtHelper.getUserId(token);
        System.out.println(userId);
        //根据用户id查询数据库 把用户信息获取出来
        TbUser tbUser = tbUserService.getById(userId);


        //根据当日日期和用户id 查询当日签到信息，在此基础做更新
        LambdaQueryWrapper<TbAttendance> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TbAttendance::getKqdate,LocalDate.now());
        wrapper.eq(TbAttendance::getUserId,tbUser.getId());
        TbAttendance attendance = tbAttendanceService.getOne(wrapper);

        try {
            attendance.getChkout().toString();
        } catch (Exception e) {
            //把签退时间设为当前
            attendance.setChkout(LocalTime.now());

            //判断签退情况
            TbDept dept = tbDeptService.getById(tbUser.getDeptId());
            if(LocalTime.now().isBefore(dept.getChkout())){
                attendance.setResult(attendance.getResult() + "、早退");
            }else{
                attendance.setResult("正常");
            }

            if(tbAttendanceService.updateById(attendance)){
                return Result.ok("签退成功");
            }else {
                return Result.fail();
            }



        }
        return Result.fail("禁止重复签退");


    }

}

