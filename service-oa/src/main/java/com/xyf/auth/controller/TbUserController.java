package com.xyf.auth.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.xyf.auth.entity.TbUser;
import com.xyf.auth.service.TbUserService;
import com.xyf.auth.utils.UUIDUtil;
import com.xyf.common.result.Result;
import com.xyf.common.utils.MD5;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */

@Api(tags = "用户管理")
@RestController
@RequestMapping("/admin/system/tbUser")
public class TbUserController {
    @Autowired
    private TbUserService sysUserService;



    @ApiOperation(value = "获取用户")
    @GetMapping("get/{id}")
    public Result get(@PathVariable Long id) {
        TbUser user = sysUserService.getById(id);
        return Result.ok(user);
    }

    @ApiOperation(value = "保存用户")
    @PostMapping("save")
    public Result save(@RequestBody TbUser user) {
        //生成uudi
        String uuid = UUIDUtil.getUUID();
        user.setId(uuid);
        //密码进行加密处理，使用MD5
        String passwordMD5 = MD5.encrypt(user.getPwd());
        user.setPwd(passwordMD5);

        if (sysUserService.save(user)) {
            return Result.ok();
        }else {
            return Result.fail();
        }

    }

    @ApiOperation(value = "更新用户")
    @PutMapping("update")
    public Result updateById(@RequestBody TbUser user) {
        //密码进行加密处理，使用MD5
        String passwordMD5 = MD5.encrypt(user.getPwd());
        user.setPwd(passwordMD5);
        if(sysUserService.updateById(user)){
            return Result.ok();
        }else{
            return Result.fail();
        }

    }

    @ApiOperation(value = "删除用户")
    @DeleteMapping("remove/{id}")
    public Result remove(@PathVariable Long id) {
        if(sysUserService.removeById(id)){
            return Result.ok();
        }else {
            return Result.fail();
        }

    }
}

