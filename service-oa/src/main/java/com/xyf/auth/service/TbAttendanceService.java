package com.xyf.auth.service;

import com.xyf.auth.entity.TbAttendance;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
public interface TbAttendanceService extends IService<TbAttendance> {

}
