package com.xyf.auth.service.impl;

import com.xyf.auth.entity.TbUser;
import com.xyf.auth.mapper.TbUserMapper;
import com.xyf.auth.service.TbUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
@Service
public class TbUserServiceImpl extends ServiceImpl<TbUserMapper, TbUser> implements TbUserService {

}
