package com.xyf.auth.service.impl;

import com.xyf.auth.entity.TbAttendance;
import com.xyf.auth.mapper.TbAttendanceMapper;
import com.xyf.auth.service.TbAttendanceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
@Service
public class TbAttendanceServiceImpl extends ServiceImpl<TbAttendanceMapper, TbAttendance> implements TbAttendanceService {

}
