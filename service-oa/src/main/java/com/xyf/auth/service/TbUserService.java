package com.xyf.auth.service;

import com.xyf.auth.entity.TbUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
public interface TbUserService extends IService<TbUser> {

}
