package com.xyf.auth.service.impl;

import com.xyf.auth.entity.TbDept;
import com.xyf.auth.mapper.TbDeptMapper;
import com.xyf.auth.service.TbDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
@Service
public class TbDeptServiceImpl extends ServiceImpl<TbDeptMapper, TbDept> implements TbDeptService {

}
