package com.xyf.auth.service;

import com.xyf.auth.entity.TbDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyf
 * @since 2023-07-27
 */
public interface TbDeptService extends IService<TbDept> {

}
