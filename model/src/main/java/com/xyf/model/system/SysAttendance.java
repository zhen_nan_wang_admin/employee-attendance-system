package com.xyf.model.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xyf.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;

import java.util.Date;


@Data
@ApiModel(description = "考勤")
@TableName("sys_dept")
public class SysAttendance extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "人员表外键")
    @TableField("user_id")
    private String userId;


    @ApiModelProperty(value = "结果")
    @TableField("result")
    private String result;


    @ApiModelProperty(value = "考勤时间")
    @TableField("kqdate")
    private DateTimeLiteralExpression.DateTime kqdate;



    @ApiModelProperty(value = "上班考勤时间")
    @TableField("chkin")
    private Date chkin;



    @ApiModelProperty(value = "下班考勤时间")
    @TableField("chkout")
    private Date chkout;
}
