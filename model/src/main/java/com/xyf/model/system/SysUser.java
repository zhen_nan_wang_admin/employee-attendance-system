package com.xyf.model.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.xyf.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "用户")
@TableName("sys_user")
public class SysUser extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "人员名称")
	@TableField("name")
	private String name;

	@ApiModelProperty(value = "密码")
	@TableField("pwd")
	private String pwd;

	@ApiModelProperty(value = "账户名")
	@TableField("username")
	private String username;



	@ApiModelProperty(value = "部门id")
	@TableField("dept_id")
	private Long deptId;


	@TableField(exist = false)
	private List<SysRole> roleList;
	//岗位
	@TableField(exist = false)
	private String postName;
	//部门
	@TableField(exist = false)
	private String deptName;
}

